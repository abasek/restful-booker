import config

ping = f"{config.base_url}/ping"
bookings = f"{config.base_url}/booking"
auth = f"{config.base_url}/auth"


def booking_with_id(booking_id: str) -> str:
    return f"{bookings}/{booking_id}"


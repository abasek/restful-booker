import pytest as pytest
import random
import requests
from requests import Session
import endpoints
import config
from helpers import generate_random_date, generate_random_string


@pytest.fixture
def new_booking_body() -> dict:
    new_booking = {
        "firstname": generate_random_string("firstname"),
        "lastname": generate_random_string("lastname"),
        "totalprice": random.randint(1, 10000),
        "depositpaid": bool(random.getrandbits(1)),
        "bookingdates": {
            "checkin": generate_random_date(),
            "checkout": generate_random_date()
        },
        "additionalneeds": generate_random_string(limit=100)
    }
    return new_booking


@pytest.fixture
def new_booking_id(new_booking_body: dict, authorized_session: Session) -> str:
    response = requests.post(url=endpoints.bookings, json=new_booking_body)
    response_body = response.json()
    returned_booking_id = response_body["bookingid"]
    yield returned_booking_id
    authorized_session.delete(endpoints.booking_with_id(returned_booking_id))


@pytest.fixture(scope="session")
def new_token() -> str:
    token_body = {
        "username": config.user_name,
        "password": config.password
    }
    response = requests.post(url=endpoints.auth, json=token_body)
    response_body = response.json()
    return response_body["token"]


@pytest.fixture(scope='session')
def authorized_session(new_token: str) -> Session:
    session = Session()
    session.headers = {"Cookie": "token=" + new_token, "Accept": "application/json"}
    return session

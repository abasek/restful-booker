from assertpy import assert_that
import requests
import pytest
import endpoints
from helpers import update_booking_body


def test_ping_check_status_code_equals_201():
    response = requests.get(url=endpoints.ping)
    assert_that(response.status_code).is_equal_to(201)


def test_create_booking_check_data_based_on_variables(new_booking_body, new_booking_id):
    response = requests.post(url=endpoints.bookings, json=new_booking_body)
    response_body = response.json()
    assert_that(response.status_code).is_equal_to(200)
    assert_that(response_body).contains_key("bookingid")
    assert_that(response_body["booking"]).is_equal_to(new_booking_body)
    response_get = requests.get(url=endpoints.booking_with_id(new_booking_id))
    response_get_body = response_get.json()
    assert_that(response_get.status_code).is_equal_to(200)
    assert_that(response_get_body).is_equal_to(new_booking_body)


@pytest.mark.parametrize("total_price, expected_status_code",
                         [("100", 200), ("-100", 200), ("100,00", 200), ("dupa", 400)])
def test_create_booking_check_data_validation_total_price(new_booking_body, total_price, expected_status_code):
    new_booking_body["totalprice"] = total_price
    response = requests.post(url=endpoints.bookings, json=new_booking_body)
    assert_that(response.status_code).is_equal_to(200)


def test_get_booking_check_status_code_200(new_booking_id):
    response = requests.get(url=endpoints.booking_with_id(new_booking_id))
    assert_that(response.status_code).is_equal_to(200)


@pytest.mark.parametrize("param_name", ["firstname", "lastname"])
def test_get_booking_ids_check_data_parameters_validation_names(new_booking_body, param_name, new_booking_id):
    response = requests.get(url=endpoints.bookings, params={param_name: new_booking_body[param_name]})
    response_body = response.json()
    assert_that(response.status_code).is_equal_to(200)
    assert_that(response_body).is_equal_to([{"bookingid": new_booking_id}])


# test will check only dates that are being added during it as there is no possibility to clean the api db
@pytest.mark.parametrize("param_name", ["checkin", "checkout"])
def test_get_booking_ids_check_data_parameters_validation_dates(new_booking_body, param_name, new_booking_id):
    response = requests.get(url=endpoints.bookings, params={param_name: new_booking_body["bookingdates"][param_name]})
    response_body = response.json()
    assert_that(response.status_code).is_equal_to(200)
    assert_that(response_body).contains({"bookingid": new_booking_id})


@pytest.mark.parametrize("param_name", ["firstname", "lastname", "additionalneeds", "totalprice", "depositpaid"])
def test_update_booking_check_data(new_booking_body, new_booking_id, authorized_session, param_name):
    updated_booking_body = new_booking_body
    update_booking_body(updated_booking_body, param_name)
    response_update = authorized_session.put(url=endpoints.booking_with_id(new_booking_id), json=updated_booking_body)
    response_update_body = response_update.json()
    assert_that(response_update.status_code).is_equal_to(200)
    assert_that(response_update_body[param_name]).is_equal_to(updated_booking_body[param_name])

    response_get = requests.get(url=endpoints.booking_with_id(new_booking_id))
    response_get_body = response_get.json()
    assert_that(response_get.status_code).is_equal_to(200)
    assert_that(response_get_body[param_name]).is_equal_to(updated_booking_body[param_name])


@pytest.mark.parametrize("param_name", ["checkin", "checkout"])
def test_update_booking_check_dates(new_booking_body, new_booking_id, authorized_session, param_name):
    updated_booking_body = new_booking_body
    update_booking_body(updated_booking_body, param_name)
    response_update = authorized_session.put(url=endpoints.booking_with_id(new_booking_id), json=updated_booking_body)
    response_update_body = response_update.json()
    assert_that(response_update.status_code).is_equal_to(200)
    assert_that(response_update_body["bookingdates"][param_name]).is_equal_to(updated_booking_body["bookingdates"][param_name])

    response_get = requests.get(url=endpoints.booking_with_id(new_booking_id))
    response_get_body = response_get.json()
    assert_that(response_get.status_code).is_equal_to(200)
    assert_that(response_get_body["bookingdates"][param_name]).is_equal_to(updated_booking_body["bookingdates"][param_name])


def test_delete_booking(new_booking_body, new_booking_id, authorized_session):
    response_delete = authorized_session.delete(url=endpoints.booking_with_id(new_booking_id))
    assert_that(response_delete.status_code).is_equal_to(201)
    response_get = requests.get(url=endpoints.booking_with_id(new_booking_id))
    assert_that(response_get.status_code).is_equal_to(404)


def test_delete_booking_without_authorization(new_booking_body, new_booking_id):
    response_delete = requests.delete(url=endpoints.booking_with_id(new_booking_id))
    assert_that(response_delete.status_code).is_equal_to(403)


def test_update_booking_without_authorization(new_booking_body, new_booking_id):
    updated_booking_body = new_booking_body
    updated_booking_body["firstname"] = updated_booking_body["firstname"] + "_new"
    response_update = requests.put(url=endpoints.booking_with_id(new_booking_id), json=updated_booking_body)
    assert_that(response_update.status_code).is_equal_to(403)

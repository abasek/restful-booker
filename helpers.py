import datetime
import json
import string
import random

from requests import Response


def generate_random_date() -> str:
    start_date = datetime.date(2020, 1, 1)
    end_date = datetime.date(2020, 2, 1)
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    random_date = start_date + datetime.timedelta(days=random_number_of_days)
    return random_date.strftime("%Y-%m-%d")


def generate_random_string(prefix="", limit=30) -> str:
    letters = string.ascii_letters
    random_string = ''.join(random.choice(letters) for i in range(limit))
    return prefix + random_string


def print_response_request_as_json(response: Response):
    print(json.dumps(json.loads(response.request.body), indent=4))


def print_response_json(response: Response):
    print(json.dumps(response.json(), indent=4))


def update_booking_body(updated_booking_body: dict, param_name: str) -> dict:
    if param_name in ["firstname", "lastname", "additionalneeds"]:
        updated_booking_body[param_name] = updated_booking_body[param_name] + "_new"
    elif param_name == "totalprice":
        updated_booking_body[param_name] = updated_booking_body[param_name] + 1
    elif param_name == "depositpaid":
        updated_booking_body[param_name] = not updated_booking_body[param_name]
    elif param_name in ["checkin", "checkout"]:
        updated_booking_body["bookingdates"][param_name] = "9" + updated_booking_body["bookingdates"][param_name][1:]
    return updated_booking_body
